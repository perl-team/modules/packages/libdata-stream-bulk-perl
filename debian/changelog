libdata-stream-bulk-perl (0.11-2) UNRELEASED; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- gregor herrmann <gregoa@debian.org>  Thu, 05 Jul 2012 12:23:51 -0600

libdata-stream-bulk-perl (0.11-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:34:42 +0100

libdata-stream-bulk-perl (0.11-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Imported Upstream version 0.11.
  * Bumped year of upstream copyright.
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Added dependency on libpath-class-perl, build-dependency on
    libtest-requires-perl, perl (>= 5.10.1) | libtest-more-perl (>= 0.88).
  * Added myself to Uploaders and copyright.

  [ gregor herrmann ]
  * Remove build dependency on libtest-use-ok-perl.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 04 Mar 2012 23:46:48 +0100

libdata-stream-bulk-perl (0.08-1) unstable; urgency=low

  * New upstream release.
  * Bump build-dep on libmoose-perl to >= 0.89 for "-excludes".
  * No longer run POD tests; remove build-dep on libtest-pod-perl.
  * debian/copyright: Formatting changes for current DEP-5 proposal; refer to
    /usr/share/common-licenses/GPL-1; update years of copyright; refer to
    "Debian systems" instead of "Debian GNU/Linux systems".
  * Use debhelper compat level 8.
  * Bump Standards-Version to 3.9.1.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 28 Aug 2010 23:33:35 +0900

libdata-stream-bulk-perl (0.07-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release 0.05
  * Patch removed because it is fixed upstream
  * Added /me to Uploaders
  * Now tests pod (TEST_POD=1), but debhelper 7.0.50 is needed now

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * build depend on libtest-tempdir-perl instead of patching to not need
    it, it is now packaged
  * Update jawnsy's email address
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * New upstream release 0.06.
  * Use minimal debian/rules.
  * Add myself to Uploaders.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * New upstream release 0.07.
  * Add /me to Uploaders.
  * Set Standards-Version to 3.8.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 10 Feb 2010 01:48:50 +0100

libdata-stream-bulk-perl (0.04-1) unstable; urgency=low

  [ gregor herrmann ]
  * Remove libtest-use-ok-perl from Depends, since it's only needed for
    building the package; thanks to Bryan Donlan for the bug report
    (closes: #522668).

  [ Ryan Niebur ]
  * New upstream release
  * Debian Policy 3.8.1
  * refresh patches

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 24 Apr 2009 08:28:51 -0700

libdata-stream-bulk-perl (0.03-1) unstable; urgency=low

  * Initial release. (Closes: #516390: ITP: libdata-stream-bulk-perl --
    N at a time iteration API)

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 20 Feb 2009 23:35:00 -0800
